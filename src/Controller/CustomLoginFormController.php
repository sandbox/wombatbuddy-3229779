<?php

namespace Drupal\custom_login_form\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CustomLoginFormController.
 */
class CustomLoginFormController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    return $instance;
  }

  /**
   * Rendercustomloginform.
   *
   * @return string
   *   Return Hello string.
   */
  public function renderCustomLoginForm() {
    $build['#theme'] = 'custom_login_form';
    $build['#attached']['library'][] = 'custom_login_form/custom_login_form';
    return $build;
  }
  
}

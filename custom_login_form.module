<?php

/**
 * @file
 * Contains custom_login_form.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_menu_alter().
 *
 * Alter "Log in" and "Register" links to they open our custom login form.
 */
function custom_login_form_link_alter(&$variables) {
  $url = $variables['url'];

  if (!$url->isRouted()) {
    return;
  } 

  $route_name = $url->getRouteName();

  if ($route_name == 'user.login' || $route_name == 'registration_link.register') {

    $variables['url'] = Url::fromRoute('custom_login_form');
    $variables['options']['attributes']['class'] = [
      'use-ajax',
      'custom_login_form',
    ];
    $variables['options']['attributes']['data-dialog-type'] = 'modal';
  }
}

/**
 * Implements hook_page_attachments().
 *
 * To open the custom login form in modal dialog we should attach the 
 * "drupal.dialog.ajax" library. We do it only for anonymous users.
 */
function custom_login_form_page_attachments(&$attachments) {
  if (\Drupal::currentUser()->isAnonymous()) {
    $attachments['#attached']['library'][] = 'core/drupal.dialog.ajax';
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for user_login_form.
 */
function custom_login_form_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) { 
  // Add a class for theming a login button to render it as the image.
  $form['actions']['submit']['#value'] = '';
  $form['actions']['submit']['#attributes']['class'] = ['user-login-submit-button'];
  // Attach our library to apply CSS rules on 'user_login_form'.
  $form['#attached']['library'][] = 'custom_login_form/custom_login_form';
}

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_custom_login_form(&$variables) {
  $variables['host'] = \Drupal::request()->getSchemeAndHttpHost();
  $module_handler = \Drupal::service('module_handler');
  $variables['module_path'] = $module_handler->getModule('custom_login_form')->getPath();

  $variables['login_form'] = FALSE;

  if (!\Drupal::currentUser()->id()) {
    $form = Drupal::formBuilder()->getForm(Drupal\user\Form\UserLoginForm::class); 
    $render = Drupal::service('renderer');
    $variables['login_form'] = $render->renderPlain($form);
  }
}

/**
 * Implements hook_theme().
 */
function custom_login_form_theme() {
  return [
    'custom_login_form' => [
      'variables' => [
        'social_networks' => NULL,
        'destination' => NULL,
        'login_form' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_help().
 */
function custom_login_form_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the custom_login_form module.
    case 'help.page.custom_login_form':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides custom login form with social buttons.') . '</p>';
      return $output;

    default:
  }
}